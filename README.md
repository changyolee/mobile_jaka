# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* jaka_moveit_config, jaka_description
* Version_1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* if you want to use gazebo, "roslaunch mobile_jaka_arm cafe.launch
- to use moveit, "roslaunch mobile_moveit_config jaka_planning_execute.launch"


* if try to use it on real_robot, 
- it should change joint name as actual joint in  /mobile_moveit_config/config/controller.yaml
- it should change sensor config according to the real image topic in  /mobile_moveit_config/config/sensor_rgbd.yaml
- when use caster of move_base, just change urdf file in the caster_base/launch/move_base.launch

* all the function is seted  such as lidar, ultrasonic, camera, gripper for simulation.

### Contribution guidelines ###

* Writing tests
* positioninterface is unstable, so may it is not moveing robot arm.
* if only use gazebo, recommand change positioninterface to effortinterface then it will work.
- all positioninterface of joint and gripper in urdf, change to effortinterface
- change type in controller.yaml in mobile_jaka_arm/config


### Who do I talk to? ###

* SJTU industrial robot lab mates
* Other community or team contact